﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;




public class CaManager : MonoBehaviour
{
	//SET UP STUFF: IGNORE!
	[Header("Load CSV file of CA patterns here")]
	public TextAsset[] CAPatternsFromCSV;
    public static bool hasUpdated = true;
    public static bool hasCleaned = true;

     HashSet<int> DeadCellRules;
	 HashSet<int> AliveCellRules;

     HashSet<int> mDeadCellRules;
     HashSet<int> mAliveCellRules;

    public static trimVoxel[,] CA;

	[Header("CA Parameters")]
	//The length of time between CA calculation turns
	public float refreshrate = 0f;
	float elapseTime = 0;

	//How quick do Voxels fall out of the sky?
    public int deathRate;

	//How far does the local rule modifier bubble extend?
    public int ruleModifierRange = 2;

	//How large are vovels compared to the 1m landscape grid?
	public static float grainSize;
	public float setGrainSize;

	//How high can voxels go?
	//Voxels have a 'Value' out of 255
	public int maxValue;
	//this value is divided by the maxYheight to produce a Y position (ie. height) for each voxel
	public int maxYheight = 10;

	//How high can the player's sickness infect voxels?
	public int SicknessElev;

	public bool randomSeed;
	public bool localisedSpecialRules;
	public bool SpecialRulesAroundPlayer;
	bool hasModified = false;
  
	[Header("Player's position in the CA grid")]
	//So we can find where the player is
	//player's location in indicies
	public int playerIndiceX;
	public int playerIndiceZ;
	Vector3 playerPosPrev;
	public static Vector3 playerPos;
	public static int playerX;
	public static int playerZ;
	GameObject player;

	//IGNORE debug stuff
    Stopwatch timer = new Stopwatch();
    double tCA, tneighbours, trules, tupdate = 0;

	[Header("What Landscape attributes are we using?")]
	public int dollarMultiplierAtt; //which attribute from the landscape team are we using for the $ multiplier?
	public int specialRulesAtt; //which attribute are we choosing to base the special rules off?

	[Header("Voxel population settings")]
	//How many types of voxels are there?
	public int setNTypes;
	public static int nTypes;

	//What's the population of each voxel type?
	public static int[] typePop;
	public int[] NStypePop;

	[Header("Total $$$ each voxel type owns")]
	//CA DOLLAR CALCS//
	public float[] dollarsPerType; //Array that stores the total $$ each voxel type owns
	public static float[,] priceMultiAtXZ; //Array that stores the landscape value multiplier in XZ
	public static float[] priceMultiAtI; //Array that stores the landscape value multiplier in I

	//HOSTILE / FERTILE ENVIRONMENTS
	float[] specialRulesHere; //array that stores the landscape special rules in XZ

	//updated CA
	public static Dictionary<string, Vector2[]> CAshapes;
	public static string[] shapes;

	[Header("Debug Stuff")]
	//debug - these set landscape value multipliers to 1
	public string[] Loadedshapes;
	public float[] testMultipliers; 
	public float setTestMultipliers;

	[Header("Fun Stuff!")]
	public bool isPaperSciRoc;
	public static int PapSciRoc = 1;

	public static int Sandbox;
	public int setSandbox;



    Vector2[] glider = new Vector2[] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(0, 2), new Vector2(1, 2), new Vector2(2, 1) };

    // Use this for initialization
    void Awake()
    {
        
		grainSize = setGrainSize;

		CAshapes = new Dictionary<string, Vector2[]> ();
		CAPatternsFromCSV.loadCApattern(ref CAshapes);

		// load the names of the patterns into an array
		shapes = new string[CAshapes.Keys.Count];
		Loadedshapes = new string[CAshapes.Keys.Count];
		int count =0;
		foreach (KeyValuePair<string, Vector2[]> keyValue in CAshapes)
		{
			shapes[count] = keyValue.Key;
			Loadedshapes [count] = keyValue.Key;
			count++;
		}

		player = gameObject.GetComponent<clampMovement> ().player;

		Sandbox = setSandbox;

    }

    void Start()
    {
		//Create a grid of trimVoxels for CA calcs
		CA = new trimVoxel[landscapeManager.gridXDim, landscapeManager.gridZDim]; 
		CA.populateCAZero(); //These trimVoxels initially need all their attributes set to default values (to 0)


		nTypes = setNTypes; //how many types of voxels do we have?

		dollarsPerType = new float[nTypes + 1]; //array that stores how much money each voxel type has

		//regular CA rules
		DeadCellRules = new HashSet<int> { 3 };
        AliveCellRules = new HashSet<int> { 2, 3 };

		//modified CA rules
		mDeadCellRules =  new HashSet<int>(DeadCellRules);
		mAliveCellRules = new HashSet<int>(AliveCellRules);

		//CA Dollar
		typePop = new int[nTypes + 1];
		NStypePop = new int[nTypes + 1];
		NStypePop = typePop;

		if (isPaperSciRoc) 
		{
			PapSciRoc = 3;
			refreshrate = 0;
		}


		//DEBUG CA DOLLAR CODE
		/*
			 
			testMultipliers = new float[tileManager.gridXDim * tileManager.gridZDim];

			for (int i = 0; i < (tileManager.gridXDim * tileManager.gridZDim); i++) 
			{
				testMultipliers [i] = setTestMultipliers;
			}

			*/

		//Set up price multipliers
		priceMultiAtI = new float[landscapeManager.gridXDim * landscapeManager.gridZDim];
		specialRulesHere = new float[landscapeManager.gridXDim * landscapeManager.gridZDim];


		// ///////////////////////////////////////////////////////////////////
		// REFCA01 SEEDING LANDSCAPE RANDOMLY
		// ///////////////////////////////////////////////////////////////////

		if (randomSeed == true) 
		{

			for (int i = 0; i < 200; i++)  // i < ______ is how many starting CA patterns will be seeded
			{
				Vector3 pos = new Vector3 (Random.Range (0, landscapeManager.gridXDim), 0, Random.Range (0, landscapeManager.gridZDim));

				//CA.addIndicieShape (pos, CaManager.CAshapes [shapes [Random.Range (0, shapes.Length)]], Random.Range (0, nTypes));
				CA.addIndicieShape (pos, CaManager.CAshapes [shapes [Random.Range (0, shapes.Length)]], Random.Range (0, 3));

			}
		}

    }


    // Update is called once per frame
    void Update()
    {
		createSickness();

		grainSize = setGrainSize;

        if (SpecialRulesAroundPlayer)
        {
            Invoke("returnToRegularRules", 3);
        }
       
		//limit CA calc update to specific time intervals
        elapseTime += Time.deltaTime;
        if (elapseTime > refreshrate)
        {

            if (hasUpdated)
            {
                hasUpdated = false;
                updateCA();
            }
           
			hasUpdated = true;

            elapseTime = 0;
        }
			
		playerPos = new Vector3(Mathf.Round(player.transform.position.x) * 1/grainSize * 1/grainSize, 0, Mathf.Round(player.transform.position.z) * 1/grainSize * 1/grainSize);
		playerX = (int)playerPos.x;
		playerZ = (int)playerPos.z;

		playerIndiceX = playerX;
		playerIndiceZ = playerZ;
		playerPosPrev = playerPos;

    }

    public void updateCA()
    {
        //debug timers
		tCA = 0; tneighbours = 0; trules = 0; tupdate = 0;
        timer.Reset();
        timer.Start();

        int cellVal;
        int nx, nz;
        trimVoxel[] neighbours = new trimVoxel[8];
        int[] typeFreq = new int[nTypes];
        int[] valueSum = new int[nTypes];
        int neighbourSickness = 0;

		int indicie;


		//check entire grid
        for (int x = 0; x < landscapeManager.gridXDim; x++)
        {
            for (int z = 0; z < landscapeManager.gridZDim; z++)
            {
                tneighbours -= timer.Elapsed.TotalMilliseconds;

                int aliveNeighbourCount = 0;

                //the cell being checked
                cellVal = CA[x, z].Value; //CAvalues[x, z];
	
				indicie = CustomExtensions.convertXZtoI (x, z);
				specialRulesHere [indicie] = gameObject.GetComponent<xyzToI> ().getAttributePlus ((specialRulesAtt + 1), indicie);

                // Check neighbours values
                for (int i = -1; i <= 1; i++)
                {
                    for (int k = -1; k <= 1; k++)
                    {
                        if (!(i == 0 && k == 0))
                        {
                            nx = x + i; //nx is the x indicie location of the neighbouring cell being checked
                            nz = z + k; //nz is the z indicie location of the neighbouring cell being checked

                            if (nx < landscapeManager.gridXDim && nx >= 0 && nz >= 0 && nz < landscapeManager.gridZDim)
                            {
                                // neighbours[count] = CA[nx, nz];
                                // count++;
                                if (CA[nx, nz].Value > 0) //CAvalues[nx, nz] > 0)
                                {
                                    aliveNeighbourCount += 1; // Alive neighbour detected, add one to count
                                    typeFreq[CA[nx, nz].type] += 1;
                                    valueSum[CA[nx, nz].type] += CA[nx, nz].Value;
                                    neighbourSickness += CA[nx, nz].sickness;
                                }

                            }


                        }
                    }
                }

                //Debug stopwatches
				tneighbours += timer.Elapsed.TotalMilliseconds;
                trules -= timer.Elapsed.TotalMilliseconds;

				// ///////////////////////////////////////////////////
                // REFCA02 Rules ///////////////////////////////////////////
			    // ///////////////////////////////////////////////


				//EXAMPLE: these rules are used only for cells with a tile attribute > 0.5
				if (localisedSpecialRules == true) 
				{
					//FERTILE AREAS RULES
					if (specialRulesHere [indicie] == 1) 
					{
						DeadCellRules.Clear ();
						AliveCellRules.Clear ();

						///////////////////insert rules below //////////////////
						/// ////////////////////////////////////////////////////

						////////////How many neighbours a cell must have to become alive
						DeadCellRules.Add (0);
						DeadCellRules.Add (1);
						DeadCellRules.Add (2);
						DeadCellRules.Add (7);
						//DeadCellRules.Add (4);

						//////////How many neighbours a cell must have to die
						//AliveCellRules.Add (1);
						//AliveCellRules.Add (3);

						hasModified = true;
					}

					//HOSTILE ENIVORNMENT RULES
					if (specialRulesHere [indicie] > 0 && specialRulesHere [indicie] < 1) 
					{
						DeadCellRules.Clear ();
						AliveCellRules.Clear ();

						/////////////////////insert rules below//////////////

						//How many neighbours a cell must have to become alive
						DeadCellRules.Add (2);
						DeadCellRules.Add (7);
						//DeadCellRules.Add (4);

						//How many neighbours a cell must have to die
						AliveCellRules.Add (1);
						//AliveCellRules.Add (3);

						hasModified = true;
					}

					////////////
					/// 

					//DEBUG: Not working, ignore for now!
					//EXAMPLE: these rules would only be used for Type 1 cells
					/*
				else if (CA [x, z].type == 0) 
				{
					DeadCellRules.Clear ();
					AliveCellRules.Clear ();

					//insert rules here///////////////

					//How many neighbours a cell must have to become alive
					DeadCellRules.Add (2);
					DeadCellRules.Add (1);
					DeadCellRules.Add (4);

					//How many neighbours a cell must have to die


					hasModified = true;
				}
				*/

				}
					
                // first check if local modified CA Environment applies to this cell
                if (SpecialRulesAroundPlayer && (Mathf.Abs(playerX - x) <= ruleModifierRange) && (Mathf.Abs(playerZ - z) <= ruleModifierRange))
                {
                    // Modified CA Environment /////////////////////////////////////
                    // Dead cell rules applied (these dead cells become alive)
                    if (cellVal <= 0 && mDeadCellRules.Contains(aliveNeighbourCount))
                    {
						CA[x,z] = CA [x, z].CAinheritance (typeFreq, valueSum, 0,deathRate, maxValue);
                    }
                    // Alive Cell rules applied (these alive cells die)
                    else if (cellVal > 0 && (AliveCellRules.Contains(aliveNeighbourCount)))
                    {
                        cellVal = 0;
                        CA[x, z].NextValue = cellVal; //NextCAvalues[x, z] = cellVal;

                    }

                    // Alive Cell rules applied (these alive cells continue to be alive)
                    else if (true)
                    {
                        cellVal = (cellVal > maxValue) ? maxValue : cellVal + deathRate * 10; //if it is greater than 255, make it 255, else....
                        CA[x, z].NextValue = cellVal; //NextCAvalues[x, z] = cellVal;
                    }
                }

                else
                {
                    // Regular CA Environment  ///////////////////////////////////////// 
                    // Dead cell rules applied (these dead cells become alive)

                    if (cellVal <= 0 && DeadCellRules.Contains(aliveNeighbourCount))
                    {

						CA[x,z] = CA [x, z].CAinheritance (typeFreq, valueSum, neighbourSickness, deathRate, maxValue);

                    }
                    // Alive Cell rules applied (these alive cells die)
                    else if (cellVal > 0 && !(AliveCellRules.Contains(aliveNeighbourCount)))
                    {
                        cellVal = 0;
                        CA[x, z].NextValue = cellVal; //NextCAvalues[x, z] = cellVal;
                        CA[x, z].sickness = 0; 

                    }

                    // Alive Cell rules applied (these alive cells continue to be alive)
                    else if (cellVal > 0)
                    {
						CA [x, z].NextValue = CA [x, z].CAvalueSampling (typeFreq, valueSum, deathRate, maxValue);
						//if the there are neighbours of the same type, make this cell an average of those neighbours values, otherwise keep it the same.

						if (neighbourSickness > 0)
						{
							CA[x, z].sickness++;
						}
                    }
                }


                /////////////// Reset values ///////////////////////////////////////////

                for (int i = 0; i < typeFreq.Length + 1; i++)
                {
					if (i < typeFreq.Length) 
					{
						typeFreq[i] = 0;
						valueSum[i] = 0;
						neighbourSickness = 0;
					}

				
					typePop [i] = 0;
					dollarsPerType [i] =  0;
                }

				// Copy default rules back.
				if (hasModified == true) {
					DeadCellRules =  new HashSet<int>(mDeadCellRules);
					AliveCellRules = new HashSet<int>(mAliveCellRules);
				}

                trules += timer.Elapsed.TotalMilliseconds;

            }
        }

        tupdate -= timer.Elapsed.TotalMilliseconds;


        // update the new CA values ///////////////////////////////////////////


        //new cell values replace old cell values
        for (int x = 0; x < landscapeManager.gridXDim; x++)
        {
            for (int z = 0; z < landscapeManager.gridZDim; z++)
            {
                //make cells sick
                if (CA[x, z].Value > 0)
                {
                    if (CA[x, z].sickness > 10)
                    {
                        CA[x, z].NextValue = 0;
                        CA[x, z].sickness = 0;
                    }

                    if (CA[x, z].sickness > 0)
                    {
                        
						CA[x, z].sickness++;

						//populate typePop with sick cells
						typePop [typePop.Length - 1]++;

                    }

                    if (CA[x, z].Value > SicknessElev)
                    {
                        CA[x, z].sickness = 0;
                    }

				
					if (CA [x, z].sickness == 0) 
					{
						//populate typePOp with CA cells this round
						typePop [CA [x, z].type]++;

					}

					/////////////////////////CA DOLLAR CALC ///////////////////////
					/// 
					if (x < landscapeManager.gridXDim && x > 1 && z < landscapeManager.gridZDim && z > 1) 
					{


						//DEBUG DOLLAR CALCS
						/*
					indicie = gameObject.GetComponent<tileManager>().convertXYZtoI (x, z);

				
					dollarsPerType [CA [x, z].type] = dollarsPerType [CA [x, z].type] + 1 * priceMultiAtI [indicie];

					*/

						//ACTUAL CODE

						indicie = gameObject.GetComponent<landscapeManager> ().convertXYZtoI (x, z);

						//priceMultiAtI [indicie] = testMultipliers [indicie];
						priceMultiAtI [indicie] = gameObject.GetComponent<xyzToI> ().getAttributePlus ((dollarMultiplierAtt + 1), indicie);

						if (CA [x, z].sickness == 0) 
						{
							dollarsPerType [CA [x, z].type] = dollarsPerType [CA [x, z].type] + priceMultiAtI [indicie];
						} 
						else if (CA [x, z].sickness > 0)
						{  //add to sickness value
							dollarsPerType [(dollarsPerType.Length - 1)] = dollarsPerType [(dollarsPerType.Length - 1)] + priceMultiAtI [indicie];
						}


					}


                }

                CA[x, z].Value = CA[x, z].NextValue;

				CA [x, z].voxPosition.y = (Mathf.Round(CA[x, z].Value / maxYheight - 1));

				
					//priceMultiAtXZ [x, z] = gameObject.GetComponent<tileManager> ().getAttribute (1, x, z);

					//indicie = gameObject.GetComponent<tileManager>().convertXYZtoI (x, z);
					//priceMultiAtI [indicie] = gameObject.GetComponent<tileManager> ().getAttributeFromIndicie (1, indicie);

					//dollarsPerType [CA [x, z].type] =  dollarsPerType [CA [x, z].type] + //(float)CA[x,z].Value;


						




            }
        }

        tupdate += timer.Elapsed.TotalMilliseconds;

        tCA = timer.Elapsed.TotalMilliseconds;

    }

    void returnToRegularRules()
    {
        SpecialRulesAroundPlayer = false;
    }

    void createSickness()
    {

		if (playerX >= 0 && playerX < landscapeManager.gridXDim && playerZ >= 0 && playerZ < landscapeManager.gridZDim) {
			
			if (CA [playerX, playerZ].Value < SicknessElev
			         && CA [playerX, playerZ].sickness == 0
			         && CA [playerX, playerZ].Value > 0) {
				CA [playerX, playerZ].sickness++;
			}
		}
    }

}



