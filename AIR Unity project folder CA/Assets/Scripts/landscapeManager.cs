﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class landscapeManager : MonoBehaviour {

	//Landscape Tile Barcode Reader Setup
	public int attDisplayedOnTile = 1;
	public GameObject tile;
	public int tileReaderXdim ;
	public int tileReaderZdim ;
	public int yHeight;

	[HideInInspector]
	public int tileCount;

	//IGNORE Landscape CSV Setup
	public TextAsset csvFile; // Reference of CSV file
	private char lineSeperater = '\n'; // It defines line seperate character
	private char fieldSeperator = ','; // It defines field seperate chracter
	public ArrayList attributes = new ArrayList ();

	public static int gridXDim = -1;
	public static int gridZDim = -1;
	public static int gridYdim = -1;

	//LANDSCAPE TILE DATA
	[HideInInspector]
	public static bool[,] isDead; //Deadzone
	[HideInInspector]
	public bool[] hasSpawned;

	public static bool showDeadZone;
	public bool setShowDeadZone;


	//Superseded
	[HideInInspector]
	public bool trace;
	[HideInInspector]
	public bool[] hasVisited;
	[HideInInspector]
	public static bool[,] isVisited;
	[HideInInspector]
	public int [] timesVisited;

	//CA Stuff
	public int XDim = -1;
	public int ZDim = -1;

	public static int nAttributes;

	void Awake ()
	{
		attributes = readData (csvFile);
	}

	// Use this for initialization
	void Start () 
	{	 

		//////////////////////////////////////
		/// SETUP FOR LANDSCAPE TILES
		/// /////////////////////////////////

		//Read data from Landscape CSV file
		XDim = gridXDim;
		ZDim = gridZDim;


		//Array recording if tile at X, Z location is in the deadzone
		isDead = new bool[gridXDim,gridZDim];

		//Array recording if tile at X, Z location has spawned a CA pattern already
		hasSpawned = new bool[attributes.Count];

		//////////////////////////////////////
		/// LANDSCAPE DATA FOR CA
		/// /////////////////////////////////

		//How many attributes has the landscape team given us?
		nAttributes = attributes.Count;

		//Superseded Landscape attributes
		hasVisited = new bool[attributes.Count];
		isVisited = new bool[gridXDim, gridZDim];
		timesVisited = new int[attributes.Count];

		for (int i = 0; i < attributes.Count; i++)
		{
			hasSpawned[i] = false;
		}

		for (int j = 0; j < gridXDim; j++)
		{
			for (int k = 0; k < gridZDim; k++)
			{
				isDead[j, k] = false;
				isVisited[j, k] = false;
			}
		}


		//Create tile barcode reader
		for (int x = -tileReaderXdim; x < tileReaderXdim; x++) 
		{
			for (int z = -tileReaderZdim; z < tileReaderZdim; z++) 
			{

				//spawning tiles
				Vector3 pos = new Vector3 (x, yHeight, z);
				Quaternion rot = Quaternion.identity;
				GameObject newObj = Instantiate (tile, pos, rot) as GameObject;
				newObj.transform.parent = gameObject.transform;
				tileCount++;

			}
		}



	}

	void Update ()
	{
		showDeadZone = setShowDeadZone;
	}



	//IGNORE
	private ArrayList readData(TextAsset csv)
	{
		// create an array list to add all the attributes to as they are read from the CSV file
		ArrayList outArrayList = new ArrayList();

		// Separate each row with the lineSeperater character
		string[] records = csv.text.Split(lineSeperater);

		// The first row is the setup information, Key/Value pair with even indicies being strings as a key and the odd the values
		string[] SetupRow = records[0].Split(fieldSeperator);

		// Loop through each key in the SetupRow, this will be the even elements of the string array, SetupRow[i]
		// that is i=0,2,4 ect so in the for loop not i++ but i=i+2
		for (int i = 0; i < SetupRow.Length; i = i + 2)
		{
			string key = SetupRow[i];
			// On each of thoes keys if they match the following do somehting with the value, which is i+1, the next value in the CSV startup row
			switch (key)
			{
			case "GridWidth":
				// The number is stored as a string not an int, so convert it to an int and give error reporting if it doesnt work
				// The tryParse will convert the string to an int, if it fails it returns false, so having the ! will exicute when failed
				if (!int.TryParse(SetupRow[i + 1], out gridXDim))
				{
					// Error message here, could not read the GridWidth!!! 
				}
				break;
			case "GridDepth":
				// The number is stored as a string not an int, so convert it to an int and give error reporting if it doesnt work
				// The tryParse will convert the string to an int, if it fails it returns false, so having the ! will exicute when failed
				if (!int.TryParse(SetupRow[i + 1], out gridZDim))
				{
					// Error message here, could not read the GridDepth!!! 
				}
				break;
			default:
				break;
			}
		}

		// For the rest of the rows read each row and split it up into the different attributes storing them in an arraylist.
		// Remember that the first row is setup so we much check from i=1 not zero!
		for (int i = 1; i < records.Length; i++)
		{
			string[] fields = records[i].Split(fieldSeperator);

			double[] newAtt = new double[fields.Length];
			// Loop for each attribute, parse (or convert) them from string to double (floating point values)
			for (int j = 0; j < fields.Length; j++)
			{
				if (!double.TryParse(fields[j], out newAtt[j]))
				{
					// error handle
				}
			}
			outArrayList.Add(newAtt);
		}

		return outArrayList;
	}

	//function
	public int convertXYZtoI(int myX, int myZ)
	{
		int indicie = myX * gridXDim + myZ;
		return indicie;
	}


	public int convertXYZtoI(Vector3 vecPos) 
	{
		return convertXYZtoI ((int) Mathf.RoundToInt (vecPos.x), (int) Mathf.RoundToInt (vecPos.z));
	}

	public int ItoX (int i)
	{
		return i % gridXDim;
	}
	public int ItoZ(int i)
	{
		return i / gridZDim;
	}

}

