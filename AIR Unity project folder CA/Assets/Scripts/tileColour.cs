﻿using UnityEngine;
using System.Collections;

public class tileColour : MonoBehaviour
{

    public GameObject obj;

    public GameObject manager;
    public GameObject player;

    public float accumColor = 0;

    Renderer myRenderer;
    public GameObject thisObject;

    public int attributeA;

    public int visitedTileNo;
    public bool slaveVisited = false;

    public int slaveTimesVisited;
    bool hasSpawned = false;

    public Vector4 colourVal;

    public int myX;
    public int myZ;

	int shapeInt;

	public int _gridXDim;
	public int _gridZDim;

	public float Treeness;
	public float Pathness;
	public float Noiseness;
	public float Interestingness;

    bool tileVis;

    // IGNORE ME //
	// Use this for initialization
    void Start()
    {
		myRenderer = gameObject.GetComponent<Renderer>();

		_gridXDim = landscapeManager.gridXDim;
		_gridZDim = landscapeManager.gridZDim;
	
    }

	// IGNORE ME
    // Update is called once per frame
    void Update()
    {
        myX = Mathf.RoundToInt(transform.position.x);
        myZ = Mathf.RoundToInt(transform.position.z);


		//////TC00 visible tiles
		/// 
		/// 
        //accumColor = attributA
		if (manager.GetComponent<landscapeManager>().attDisplayedOnTile == 1)
        {
            accumColor = thisObject.GetComponent<xyzToI>().slaveAtt1;
        }

		else if (manager.GetComponent<landscapeManager>().attDisplayedOnTile == 2)
        {
            accumColor = thisObject.GetComponent<xyzToI>().slaveAtt2;
        }

		else if (manager.GetComponent<landscapeManager>().attDisplayedOnTile == 3)
        {
            accumColor = thisObject.GetComponent<xyzToI>().slaveAtt3;
        }

		else if (manager.GetComponent<landscapeManager>().attDisplayedOnTile == 4)
		{
			accumColor = thisObject.GetComponent<xyzToI>().slaveAtt4;


		}

		///////////////////////////////////////////////////////////////////////////
		////////////////////// ATTRIBUTES REFTC01 ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////

		Treeness = thisObject.GetComponent<xyzToI>().slaveAtt1;
		Noiseness = thisObject.GetComponent<xyzToI>().slaveAtt2;
		Interestingness = thisObject.GetComponent<xyzToI>().slaveAtt3;
		Pathness = thisObject.GetComponent<xyzToI>().slaveAtt4;


		///////////////////////////////////////////////////////////////////////////
		////////////////////// END ATTRIBUTES ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////


        myRenderer.enabled = false;

        visitedTileNo = thisObject.GetComponent<xyzToI>().TileNo;

        slaveVisited = manager.GetComponent<landscapeManager>().hasVisited[visitedTileNo];
        slaveTimesVisited = manager.GetComponent<landscapeManager>().timesVisited[visitedTileNo];
        hasSpawned = manager.GetComponent<landscapeManager>().hasSpawned[visitedTileNo];

	

        if (manager.GetComponent<landscapeManager>().trace == false)
        {
            if (accumColor > 0)
            {
                myRenderer.enabled = true;
                Color myColor = new Vector4(accumColor, accumColor, accumColor, 1);
                myRenderer.material.color = myColor;
            }
        }


		/// ////////////////////////////////////////////////////////
		/////////////////////////// REFTC02 ///////////////////////////
		/// ///////////////////////////////////////////////////////
		 


		if (Pathness > 0.8f && Pathness <= 1 && hasSpawned == false) 
		{
			//WHAT COLOUR
			int patternType = 1;

			// spawn [order in shape list], deadzone, ignore, ignore)
			CaManager.shapes[1].patternAtLocation (6, gameObject.transform.position, patternType);
			hasSpawned = true;
		}
			

		if (Interestingness > 0.8f && hasSpawned == false) 
		{
			//WHAT COLOUR
			int patternType = 2;

			// spawn [order in shape list], deadzone, ignore, ignore)
			CaManager.shapes[1].patternAtLocation (6, gameObject.transform.position, patternType);
			hasSpawned = true;
		}
		 
		///////////////////////////////////////////////////////////////////////////////////
		/// ////////////////////// END REF1TC ///////////////////////////////////
		/// ////////////////////////////////////////////////////////////////////////////

		if (myX >= 0 && myX < _gridXDim && myZ >= 0 && myZ < _gridZDim) 
		{
			if (landscapeManager.isDead [myX, myZ] == true && landscapeManager.showDeadZone == true) 
			{
				myRenderer.enabled = true;
				myRenderer.material.color = Color.red;
			}
		}	

 	}



    


}