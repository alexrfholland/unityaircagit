﻿using UnityEngine;
using System.Collections.Generic;

public class voxelManager : MonoBehaviour
{
}
	
public struct trimVoxel
{
    public int Value, NextValue, type, sickness;
	public Vector3 voxPosition;
    public int[] history;

	public trimVoxel(int value, int type, int xInd, int yInd)
    {
        Value = value;
        NextValue = 0;
        this.type = type;
        history = new int[10];
        sickness = 0;
		voxPosition = new Vector3 (xInd * CaManager.grainSize, 0, yInd * CaManager.grainSize);
    }
}
		

