﻿using UnityEngine;
using System.Collections;

public class xyzToI : MonoBehaviour {

	public int myX;
	public int myZ;

	public int gridWidth;
	public int gridDepth;
	public int TileNo;

	public GameObject manager;

	public int slaveTileCount;
	public float slaveAtt1;
	public float slaveAtt2;
	public float slaveAtt3;
	public float slaveAtt4;

	public bool slaveVisited = false;
	public int slaveTimesVisited = 0;

	public bool tileWithinRange = false;

	public Vector3 XYZPoint;

	void Start () 
	{



		//gridWidth = manager.GetComponent<tileManager>().GridWidth;
		//gridDepth = manager.GetComponent<tileManager>().GridDepth;

	}

	// Update is called once per frame
	void Update ()
	{
		gridWidth = landscapeManager.gridXDim;
		gridDepth = landscapeManager.gridZDim;

		myX = Mathf.RoundToInt (transform.position.x);
		myZ = Mathf.RoundToInt (transform.position.z);

		if ((myX >= 0 && myX <= gridWidth) && (myZ >= 0 && myZ <= gridDepth)) 
		{
			tileWithinRange = true;
			TileNo = manager.GetComponent<landscapeManager> ().convertXYZtoI (transform.position);


			slaveTileCount = TileNo;
			slaveAtt1 = getAttribute (0);
			slaveAtt2 = getAttribute (1);
			slaveAtt3 = getAttribute (2);
			slaveAtt4 = getAttribute (3);

		} else 

		{
			slaveAtt1 = 0;
			slaveAtt2 = 0;
			slaveAtt3 = 0;
			slaveAtt4 = 0;
		}



	}

	public float getAttribute(int attribute) 
	{

		//error check
		//gets list all of arrays for all of tiles
		ArrayList tempList = (ArrayList)(manager.GetComponent<landscapeManager> ().attributes);

		//checks if this list is empty
		if (tempList != null && tempList.Count > 0) {
			//gets a list of attributes for this tile
			if (tempList.Count > slaveTileCount && slaveTileCount >= 0) {

				double[] tempdouble = (double[])tempList [slaveTileCount];

				//checks if this list is empty
				if (tempdouble != null && tempdouble.Length > 0) {

					//returns this attribute
					return (float)tempdouble [attribute];
				}
			}
		}
		return 0f;
	}



	public float getAttributePlus(int attribute, int indicie) 
	{

		//error check
		//gets list all of arrays for all of tiles
		ArrayList tempList = (ArrayList)(manager.GetComponent<landscapeManager> ().attributes);

		//checks if this list is empty
		if (tempList != null && tempList.Count > 0) {
			//gets a list of attributes for this tile
			if (tempList.Count > indicie && indicie >= 0) {

				double[] tempdouble = (double[])tempList [indicie];

				//checks if this list is empty
				if (tempdouble != null && tempdouble.Length > 0) {

					//returns this attribute
					return (float)tempdouble [attribute];
				}
			}
		}
		return 0f;
	}
}
