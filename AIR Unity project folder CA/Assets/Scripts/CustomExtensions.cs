﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class CustomExtensions
{


	///////////////////////////////////////////////////////////////////
	// REFCE01 Change the inheritance types
	// ///////////////////////////////////////////////////////////////////



	public static trimVoxel CAinheritance(this trimVoxel ThisVoxel,int[] typeFreq,int[] valueSum, int neighbourSickness, int deathRate, int maxValue) 
	{
	
	/*  *** indicate variables you can play with!

	*** ThisVoxel --> the current voxel we are working with

	*** ThisVoxel.type --> is the voxel's type
	*** ThisVoxel.position --> is the voxels XYZ position 
	*** ThisVoxel.position.y --> is the height of the voxel
	*** ThisVoxel.sickness --> is how sick the voxel is. Sickness > 0 is sick. Sickness > 10 and the voxel dies

___________________ EXPLAINATION OF typeFreq ARRAY ________________________________________________

	*** typeFreq[i] --> is an array (a list) of the number of neighbouring types the Voxel has
	Remember, the type of voxel is represented by a number. Ie a voxel (and its neighbours) can be a Type 0 voxel, Type 1 voxel, Type 2 voxel, etc


	Ie. 'ME' below is a voxel (ThisVoxel,  the surrounding numbers indicating an adjacent voxel neighbour of that type):

	                1  0  2
					2  ME 1
					0  1  2
	
	 The ME voxel would have the following neighbours: 
				-- 2 type zero neighbours (A type 2 neighbour to the middle-left, a type 2 neihbour to the top-right)
				-- 3 type one neighbours 
				-- 3 type two neighbours
				 

	typeFreq would record this as typeFreq[] = {2, 3, 3}. The 0th indicie indicating how many type 0 voxels neighbours are present (2 type 0 neighbours ),
	the 1st indicie how many type 1 neighbours (3), the 2nd how many type 2 neighbours (3).

______________ ACCESSING typeFreq _____________________________________________________________________

	Voxel types are represented by a number, and so each voxel type is also the indicie of how many neighbours of that type.
	
	If you would like to see how many type 1 neighbours ThisVoxel has:
	typeFreq[1] = ...... (the answer is 3!)



________________________________________________________________________________________________________

	*** valueSum --> sum of the values for all neighbouring voxels
	*** neighbourSickness --> a neighbourSickness > 0 means a neighbouring voxel is sick

__________________ METHOD SHORTUCTS____________________________________________________________________

	You also have access to some methods (formualae) I have already written. In particular:

	*** findMaxIndicie(typeFreq) --> will find the most common neighbouring voxel type
	*** findMinIndicie(typeFreq) --> will find the rarest common neighbouring voxel type

	ie. ThisVoxel.type = typeFreq.findMaxIndicie(); sets this voxel to the most common neighbouring type
		ThisVoxel.type = typeFreq.findMinIndicie(); sets this voxel to the least common neighbouring type

	*/



		// Set new type
		int cellVal = 0;
		ThisVoxel.type = typeFreq.findMaxIndicie();


		////////////REFCE04////////////
		// DO things per voxel type

		switch (ThisVoxel.type) 
		{
		case 0:
			cellVal = ThisVoxel.TypeZeroSinks (typeFreq, valueSum, deathRate, maxValue);
			break;

		default:
			cellVal = ThisVoxel.CAvalueSampling(typeFreq, valueSum, deathRate, maxValue);
			break;
		}

		ThisVoxel.NextValue = cellVal; //NextCAvalues[x, z] = cellVal;

		if (neighbourSickness > 0)
		{
			ThisVoxel.sickness = ThisVoxel.sickness + 1;

		}

		if (ThisVoxel.type == 1 && neighbourSickness > 0 && neighbourSickness < 5) 
		{
			ThisVoxel.sickness = 0;
		}


		////////////REFCE05////////////

		//Sets Voxel type 1 to least common neighbour
		if (ThisVoxel.type == 1) 
		{
			ThisVoxel.type = typeFreq.findMinIndicie ();
		}



		if (CaManager.PapSciRoc > 1) 
		{
			/////REFCE02/////////

			//PAPER SCISSCORS ROCK TEST

			// TYPE 0 LOOSES TO TYPE 1
			// If this Voxel is TYPE0 && it has a neighbour TYPE 1, make this voxel type 0

			if (ThisVoxel.type == 0 && typeFreq [1] > 0) 
			{
				ThisVoxel.type = 1;
			}

			//TYPE 1 looses to TYPE 2
			if (ThisVoxel.type == 1 && typeFreq [2] > 0) 
			{
				ThisVoxel.type = 2;
			}

			//TYPE 2 looses to TYPE 0
			if (ThisVoxel.type == 2 && typeFreq [0] > 0) 
			{
				ThisVoxel.type = 0;
			}	
		}

		if (CaManager.Sandbox == 1) 
		{
			//write your code here
		}

		if (CaManager.Sandbox == 2) 
		{
			//write your code here
		}

		if (CaManager.Sandbox == 3) 
		{
			//write your code here
		}


		return ThisVoxel;

	}



	// ///////////////////////////////////////////////////////////////////
	//    REFCE03 Change the Y position
	// ///////////////////////////////////////////////////////////////////


	//DEFAULT CASAMPLING
	// if the voxel is alive and stays alive
	public static int CAvalueSampling(this trimVoxel ThisVoxel,int[] typeFreq,int[] valueSum, int deathRate, int maxValue) {
		
		int cellVal = ThisVoxel.Value;

		// find the type of the highest average values.
		int averageVal = (typeFreq [ThisVoxel.type] != 0) ? (valueSum [ThisVoxel.type] / typeFreq [ThisVoxel.type]) : cellVal;

		if (averageVal <= deathRate) 
		{
			cellVal = deathRate;
		} 

		//if there are more than 3 neighbours, displace by random with domain of deathrate
		else if (typeFreq [ThisVoxel.type] >= 3 && cellVal < maxValue) 
		{
			cellVal = averageVal + Random.Range ((int)-deathRate / 2, deathRate + 1);
		}
		//otherwise decay by deathrate
		else 
		{
			cellVal =  averageVal - deathRate;
		}
		return cellVal; //NextCAvalues[x, z] = cellVal;

	}

	//EXAMPLE: TYPE ZERO SINKS
	public static int TypeZeroSinks (this trimVoxel ThisVoxel,int[] typeFreq,int[] valueSum, int deathRate, int maxValue) 
	{

		int cellVal = ThisVoxel.Value;
		// Get an average of all the values

		if (typeFreq [ThisVoxel.type] != 0) 
		{
			//cellVal = average of the same types of neighbours

			int chance = Random.Range (0, 10);

			if (chance > 8) 
			{
				cellVal = valueSum [ThisVoxel.type] / typeFreq [ThisVoxel.type] + deathRate;
			}

			if (chance <= 8) 
			{
				cellVal = valueSum [ThisVoxel.type] / typeFreq [ThisVoxel.type] - deathRate * 4;
			}

		}

		//CellVal does not equal or go below 0 (becomes bottom feeder)
		if (cellVal <= deathRate) 
		{
			cellVal = deathRate;
		} 
		if (cellVal > maxValue) {
			cellVal = maxValue;
		}


		return cellVal;
	
	}
		
	public static HashSet<int> ToHashSet<T>(this IEnumerable<int> source)
	{
		if (source == null)
		{
			throw new System.ArgumentNullException("source");
		}
		return new HashSet<int>(source);
	}

	public static int findMaxIndicie(this int[] intArray) 
	{
		int maxAmount = 0;
		int highestIndicie = 0;

		for (int i = 0; i < intArray.Length; i++) {
			if (intArray [i] > maxAmount) {
				maxAmount = intArray [i];
				highestIndicie = i;
			}
		}
		return highestIndicie;

	}

	public static int findMinIndicie(this int[] intArray) 
	{
		int minAmount = 1;
		int lowestIndicie = 0;

		for (int i = 0; i < intArray.Length; i++) 
		{
			if (intArray [i] != 0 && intArray [i] <= minAmount) {
				minAmount = intArray [i];
				lowestIndicie = i;
			}
		}
		return lowestIndicie;

	}

	public static int findMaxValue(this int[] intArray) {
		int maxAmount = 0;

		for (int i = 0; i < intArray.Length; i++) {
			if (intArray [i] >= maxAmount) {
				maxAmount = intArray [i];
			}
		}
		return maxAmount;
	}

	public static void populateCARandom(this float[,] CAgrid)
	{
		for (int i = 0; i < CAgrid.GetLength(0); i++)
		{
			for (int j = 0; j < CAgrid.GetLength(1); j++)
			{
				CAgrid[i, j] = Random.Range(0, 256);
			}
		}
	}

	public static void populateCAZero(this float[,] CAgrid)
	{
		for (int i = 0; i < CAgrid.GetLength(0); i++)
		{
			for (int j = 0; j < CAgrid.GetLength(1); j++)
			{
				CAgrid[i, j] = 0;
			}
		}
	}

	public static void addshape(this float[,] CAgrid, Vector3 location, Vector2[] incriments)
	{
		for (int i = 0; i < incriments.Length; i++)
		{
			int xPos = (int)(location.x + incriments[i].x);
			int zPos = (int)(location.z + incriments[i].y);
			if (xPos >= 0 && xPos < CAgrid.GetLength(0) && zPos >= 0 && zPos < CAgrid.GetLength(1))
			{
				CAgrid[xPos, zPos] = 255;
			}
		}
	}

	/// //////////////////////////////////////


	public static void populateCARandom(this trimVoxel[,] CAgrid)
	{
		for (int i = 0; i < CAgrid.GetLength(0); i++)
		{
			for (int j = 0; j < CAgrid.GetLength(1); j++)
			{
				CAgrid[i, j] = new trimVoxel(Random.Range(0, 256), 0, i, j);
			}
		}
	}

	public static void populateCAZero(this trimVoxel[,] CAgrid)
	{
		for (int i = 0; i < CAgrid.GetLength(0); i++)
		{
			for (int j = 0; j < CAgrid.GetLength(1); j++)
			{
				CAgrid[i, j] = new trimVoxel(0, 0, i, j);
			}
		}
	}

	public static void addIndicieShape(this trimVoxel[,] CAgrid, Vector3 indicieLocation, Vector2[] pattern, int type)
	{
		for (int i = 0; i < pattern.Length; i++)
		{
			int xPos = (int)(indicieLocation.x + pattern[i].x);
			int zPos = (int)(indicieLocation.z + pattern[i].y);
			if (xPos >= 0 && xPos < CAgrid.GetLength(0) && zPos >= 0 && zPos < CAgrid.GetLength(1))
			{
				CAgrid[xPos, zPos].Value = type * 10 + 20;
				CAgrid[xPos, zPos].type = type;
			}
		}
	}


	public static void loadCApattern(this TextAsset[] csv, ref Dictionary<string, Vector2[]> DicOut)
	{

		char lineSeperater = '\n'; // It defines line seperate character
		char fieldSeperator = ','; // It defines field seperate chracter

		for (int j = 0; j < csv.Length; j++)
		{



			// create an array list to add all the attributes to as they are read from the CSV file
			string keyout = "";
			List<Vector2> outArrayList = new List<Vector2>();

			// Separate each row with the lineSeperater character
			string[] records = csv[j].text.Split(lineSeperater);

			// The first row is the setup information, Key/Value pair with even indicies being strings as a key and the odd the values
			string[] SetupRow = records[0].Split(fieldSeperator);


			// Loop through each key in the SetupRow, this will be the even elements of the string array, SetupRow[i]
			// that is i=0,2,4 ect so in the for loop not i++ but i=i+2
			for (int i = 0; i < SetupRow.Length; i = i + 2)
			{
				string key = SetupRow[i];
				// On each of thoes keys if they match the following do somehting with the value, which is i+1, the next value in the CSV startup row
				switch (key)
				{
				case "Name":
					keyout = SetupRow[i + 1];
					break;
				default:
					break;
				}
			}

			// For the rest of the rows read each row and split it up into the different attributes storing them in an arraylist.
			// Remember that the first row is setup so we much check from i=1 not zero!
			for (int i = 1; i < records.Length; i++)
			{
				string[] fields = records[i].Split(fieldSeperator);

				int xval = 0, zval = 0;
				// Loop for each attribute, parse (or convert) them from string to int of the x/z values
				if (!int.TryParse(fields[0], out xval))
				{
					// error handle
					break;
				}
				if (!int.TryParse(fields[1], out zval))
				{
					// error handle
					break;
				}

				outArrayList.Add(new Vector2((float)xval, (float)zval));
			}
			DicOut.Add(keyout, outArrayList.ToArray());

		}


	}

	public static Vector3 fromGlobalPosToXZ(this Vector3 position) {
		return position * CaManager.grainSize;
	}


	public static int convertXZtoI(int myX, int myZ)
	{
		int indicie = myX * landscapeManager.gridXDim + myZ;
		return indicie;
	}


	public static void patternAtLocation (this string patternName, int deadZone, Vector3 pos, int type)
	{
		int myX = (int)pos.x;
		int myZ = (int)pos.z;
		if (myX >= 0 && myX < landscapeManager.gridXDim && myZ >= 0 && myZ < landscapeManager.gridZDim) {



			if (landscapeManager.isDead [myX, myZ] == false) {
				// make deadzone
				for (int x = myX - deadZone; x < myX + deadZone; x++) {
					for (int z = myZ - deadZone; z < myZ + deadZone; z++) {

						if (x >= 0 && x < landscapeManager.gridXDim && z >= 0 && z < landscapeManager.gridZDim) {
							landscapeManager.isDead [x, z] = true;

						}	
					}
				}

				// spawn shape


				CaManager.CA.addIndicieShape (pos.fromGlobalPosToXZ (), CaManager.CAshapes [patternName], type);

			}
		}
	}

}
