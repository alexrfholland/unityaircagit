﻿using UnityEngine;
using System.Collections;

public class tileAttribute : MonoBehaviour {

	public GameObject manager;

	public int slaveTileCount;

	public float slaveAtt1;
	public float slaveAtt2;
	public float slaveAtt3;





	// Use this for initialization


	void Awake ()
	{
		slaveTileCount = manager.GetComponent<landscapeManager> ().tileCount;
		slaveAtt1 = getAttribute (0);
		slaveAtt2 = getAttribute (1);
		slaveAtt3 = getAttribute (2);



		//slaveAtt1 = (float)((double[])((ArrayList)manager.GetComponent<tileManager> ().Att1)[slaveTileCount])[0];
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	float getAttribute(int attribute) 
	{

		//gets list all of arrays for all of tiles
		ArrayList tempList = (ArrayList)(manager.GetComponent<landscapeManager> ().attributes);

		//checks if this list is empty [error correction]
		if (tempList != null && tempList.Count > 0) 
		{
			//gets a list of attributes for this tile
			double[] tempdouble = (double[])tempList [slaveTileCount];

			//checks if this list is empty
			if (tempdouble != null && tempdouble.Length > 0) {

				//returns this attribute
				return (float)tempdouble[attribute];
			}
		}
		return 0f;

	}


}
