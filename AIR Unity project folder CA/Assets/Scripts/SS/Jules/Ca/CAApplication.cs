﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Scripts
{
    public class CAApplication : MonoBehaviour
    {

        CAEngine CA;
        VoxelMesher mesher;
        float deltaTime = 0.0f;
        int width;
        int height;
		int depth;
        [System.NonSerialized] public float thisFrameStart;
        public Material mat;

        void Start()
        {
			width = Manager.gridWidth;
			height = Manager.gridHeight;
			depth = Manager.gridDepth;

			VoxelGrid grid = new VoxelGrid(width, height, depth, new float[] { 0, 0, 0 }, new float[] { width, height, depth });
            grid.initRandom();
            CA = new CAEngine(grid);
            CA.Start();

            mesher = new VoxelMesher(grid);
            mesher.setPlayerPos(Camera.main.transform.position);
            mesher.Start();

        }

        // Update is called once per frame
        void Update()
        {
           
			//loop CA
            if (!CA.running)
            {
                //tell voxel renderer to update with the buffer
                mesher.setPlayerPos(Camera.main.transform.position);
                //can only be called after start
                mesher.buildMeshes();
                mesher.upToDate = false;
                mesher.Start();

                CA.Start();
            }

            

            print("meshing progress: "+ mesher.progressText);
            print("CA progress: " + CA.progressText);
            deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        }

        void OnPostRender()
        {
            //check to see if ready to render
            render();
        }

        public void render()
        {
            /*
            GL.PushMatrix();
            GL.Begin(GL.TRIANGLES);
            for (int i = 0; i < pts.Length; i += 3)
            {
                GL.Vertex(pts[i]);
                GL.Vertex(pts[i+1]);
                GL.Vertex(pts[i+2]);
                //removed colour - work in progress
                //GL.Color((Color)pts[i+3]);
            }
            GL.End();
            GL.PopMatrix();
            */
            // mat.SetPass(0);
            // draw mesh at the origin

            //seems like drawmesh is faster
            foreach (Mesh m in mesher.mesh)
            {
				Graphics.DrawMeshNow(m, Vector3.zero, Quaternion.identity);
            }

        }

        void OnGUI()
        {
            int w = Screen.width, h = Screen.height;

            GUIStyle style = new GUIStyle();

            Rect rect = new Rect(0, 0, w, h * 2 / 100);
            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = h * 2 / 100;
            style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            GUI.Label(rect, text, style);
        }
    }
}
