﻿using UnityEngine;
using System.Collections;

public class DrawMyMesh : MonoBehaviour {

	public Mesh mesh;
	public Material material;
	private MaterialPropertyBlock block;
	private int colorID;

	public GameObject player;

	public Vector3[,] position;


	// Use this for initialization
	void Start () 
	{
		block = new MaterialPropertyBlock ();
		colorID = Shader.PropertyToID("_Color");
	}
	
	// Update is called once per frame
	void Update () 
	{

		for (int i = -100; i < 100; i++) 
		{
				for (int k = -100; k < 100; k++) 
				{
					int chance = Random.Range (0, 2);
					if (chance == 0) 
					{
						block.SetColor (colorID, Color.red);
						Graphics.DrawMesh (mesh, new Vector3 (i, 1, k ), Quaternion.identity, material, 0, null, 0, block);
					}

					
				}
			
		}// red mesh

		// green mesh
		block.SetColor(colorID, Color.green);
		Graphics.DrawMesh(mesh, new Vector3(5, 0, 0), Quaternion.identity, material, 0, null, 0, block);

		// blue mesh
		block.SetColor(colorID, Color.blue);
		Graphics.DrawMesh(mesh, new Vector3(-5, 0, 0), Quaternion.identity, material, 0, null, 0, block);
	
	}
}
