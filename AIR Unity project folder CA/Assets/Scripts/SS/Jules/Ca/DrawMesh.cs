﻿using UnityEngine;
using System.Collections;

public class DrawMesh : MonoBehaviour {

    GameObject player;
	private Mesh mesh;
	private Mesh sicknessMesh;


	[Header("Do Voxels types have unique geometries?")]
	public bool uniqueGeom;
	public GameObject[] voxelGeomPerType = new GameObject[CaManager.nTypes];

	[HideInInspector]
	public Mesh[] meshPerType = new Mesh[CaManager.nTypes];

	[Header("What materials do each voxel type use?")]
	public GameObject Voxmesh;
	public Material[] voxelTypeMaterials = new Material[CaManager.nTypes];
	public Material sicknessMat;

	[Header("How many voxels are drawn on screen?")]
	public float setRenderDist;
	public static float renderDist;

	//IGNORE
    private MaterialPropertyBlock block;
    private int colorID;
    float cap;
	float value;


    // SETUP - IGNORE
    void Start () 
	{
        block = new MaterialPropertyBlock();
        colorID = Shader.PropertyToID("_Color");
        cap = 0;

		for (int i = 0; i < meshPerType.Length; i++) 
		{
			meshPerType [i] = voxelGeomPerType[i].GetComponent<MeshFilter> ().sharedMesh;		
		}

		mesh = Voxmesh.GetComponent<MeshFilter>().mesh;
		player = gameObject.GetComponent<clampMovement> ().player;
    }

    // Update is called once per frame
    void Update()
    {
		previewMaterials ();

		Vector3 currentPos;
		Matrix4x4 voxTransform = Matrix4x4.identity;
		renderDist = setRenderDist;

		for (int x = 0; x < landscapeManager.gridXDim; x++) 
        {
            for (int z = 0; z < landscapeManager.gridZDim; z++)
            {
				currentPos = new Vector3(x * CaManager.grainSize, 0, z * CaManager.grainSize); 
                 
				if ((new Vector3(x * CaManager.grainSize - CaManager.playerX, 0, z * CaManager.grainSize - CaManager.playerZ)).sqrMagnitude < renderDist * renderDist)
					
                {
                    value = CaManager.CA[x, z].Value;
                    if (value > cap)
                    {
						voxTransform.SetTRS(CaManager.CA[x,z].voxPosition * CaManager.grainSize, Quaternion.identity, Vector3.one * CaManager.grainSize * CaManager.grainSize *.95f * CaManager.PapSciRoc);

						if (CaManager.CA[x, z].sickness > 0)
                        {
 							Graphics.DrawMesh(mesh, voxTransform, sicknessMat , 0, null, 0, block);

                        }
                        else
                        {

							if (uniqueGeom == false) 
							{
								Graphics.DrawMesh (mesh, voxTransform, voxelTypeMaterials [CaManager.CA [x, z].type], 0, null, 0, block);
							}

							if (uniqueGeom == true) 
							{
								Graphics.DrawMesh (meshPerType [CaManager.CA [x, z].type], voxTransform, voxelTypeMaterials [CaManager.CA [x, z].type], 0, null, 0, block);
							}
                        }
                    }
                }
            }
        }
    }

	void previewMaterials()
	{
		for (int i = 0; i < voxelTypeMaterials.Length; i++) 
		{
			Vector3 currentPos = new Vector3(20 + i*2, 1, 20); 
			Graphics.DrawMesh(mesh, currentPos, Quaternion.identity, voxelTypeMaterials[i] , 0, null, 0, block);
		}
	}
}
