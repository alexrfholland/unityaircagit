﻿using UnityEngine;
using System.Collections;

public class ScoringManager : MonoBehaviour {

	public int[] score;
	public float[] dollarMultiplier;

	public float testAtt;
	public int tileX;
	public int tileZ;
	public int indicie;

	// Use this for initialization
	void Start () 
	{
		score = new int[CaManager.nTypes];

	
	}
	
	// Update is called once per frame
	void Update () 
	{

		//testAtt = gameObject.GetComponent<tileManager> ().getAttribute (1, tileX, tileZ);

		indicie = gameObject.GetComponent<landscapeManager>().convertXYZtoI (tileX, tileZ);

		testAtt = gameObject.GetComponent<xyzToI> ().getAttributePlus (0, indicie);


		//priceMultiAtI [indicie] = gameObject.GetComponent<tileManager> ().getAttributeFromIndicie (1, indicie);

		//dollarsPerType [CA [x, z].type] =  dollarsPerType [CA [x, z].type] + //(float)CA[x,z].Value;

	}

	float getAttribute(int attribute) 
	{

		//error check
		//gets list all of arrays for all of tiles
		ArrayList tempList = (ArrayList)(gameObject.GetComponent<landscapeManager> ().attributes);

		//checks if this list is empty
		if (tempList != null && tempList.Count > 0) {
			//gets a list of attributes for this tile
			if (tempList.Count > indicie && indicie >= 0) {

				double[] tempdouble = (double[])tempList [indicie];

				//checks if this list is empty
				if (tempdouble != null && tempdouble.Length > 0) {

					//returns this attribute
					return (float)tempdouble [attribute];
				}
			}
		}
		return 0f;
	}
}
