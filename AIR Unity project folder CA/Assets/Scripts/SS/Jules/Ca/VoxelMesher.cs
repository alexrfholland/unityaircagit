﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    class VoxelMesher : GenericWorker
    {
        //data
        public Vector3[] data;
        public Color[] colours;
        //buffers
        public List<Vector3> buffer;
        public List<Color> colourBuffer;
        public VoxelGrid v;
        public Vector3 playerPos;
        public bool upToDate = false;
        public float drawDist = 1000;
        public Mesh[] mesh;

        public VoxelMesher(VoxelGrid _voxelData) : base()
        {
            v = _voxelData;
        }
        
        public void setPlayerPos(Vector3 pos)
        {
            playerPos = pos;
        }

        public void setDrawDist(float distance)
        {
            drawDist = distance;
        }

        public override void run(BackgroundWorker worker)
        {
            if (!upToDate)
            {
                //generate mesh from voxel values

                //setup buffers
                int res = 1;
                float cutoff = 230;
                buffer = new List<Vector3>();
                colourBuffer = new List<Color>();

                //loop through voxel data and draw faces on threshold
                for (int z = 0; z<v.d; z++)
                {
                    for (int y = v.h; y > 0; y--)
                    {
                        for (int x = 0; x < v.w; x++)
                        {
                         //   if (Vector3.Distance(new Vector3(x, y, z), playerPos) < drawDist)
							if (Vector3.Distance(new Vector3(x, y, z), playerPos) < Manager.renderDist)
								
                            {
                                //see if there are neighbouring values if not create the faces
                                float val = v.getValue(x, y, z);
                                if (val >= cutoff)
                                {

                                    if (v.getValue((x - res), y, z) == 0)
                                    {
                                        addFace(val / 255, x, y, z, x, y, z + res, x, y + res, z + res, x, y + res, z);
                                    }
                                    if (v.getValue((x + res), y, z) == 0)
                                    {
                                        addFace(val / 255, x + res, y, z, x + res, y, z + res, x + res, y + res, z + res, x + res, y + res, z);
                                    }
                                    if (v.getValue(x, (y - res), z) == 0)
                                    {
                                        addFace(val / 255, x, y, z, x, y, z + res, x + res, y, z + res, x + res, y, z);
                                    }
                                    if (v.getValue(x, (y + res), z) == 0)
                                    {
                                        addFace(val / 255, x, y + res, z, x, y + res, z + res, x + res, y + res, z + res, x + res, y + res, z);
                                    }
                                    if (v.getValue(x, y, (z - res)) == 0)
                                    {
                                        addFace(val / 255, x, y, z, x, y + res, z, x + res, y + res, z, x + res, y, z);
                                    }
                                    if (v.getValue(x, y, (z + res)) == 0)
                                    {
                                        addFace(val / 255, x, y, z + res, x, y + res, z + res, x + res, y + res, z + res, x + res, y, z + res);
                                    }
                                }
                            }
                        }
                    }
                    worker.ReportProgress((int)(((float)z / v.d) * 100));
                }
                
            }

            //initialise arrays and move data from list buffers
            data = new Vector3[buffer.Count()];
            int i = 0;
            foreach(Vector3 p in buffer)
            {
                data[i] = p;
                i++;
            }
            colours = new Color[buffer.Count()/2];
            i = 0;
            foreach (Color c in colourBuffer)
            {
                colours[i] = c;
                i++;
            }

            upToDate = true;
        }

        private void addFace(float col, float ax, float ay, float az, float bx, float by, float bz, float cx, float cy, float cz, float dx, float dy, float dz)
        {
            //first tri
            buffer.Add(new Vector3(ax, ay, az));
            buffer.Add(new Vector3(bx, by, bz));
            buffer.Add(new Vector3(dx, dy, dz));
            //color
           // buffer.Add(new Vector3(col,col,col));
            //second tri
            buffer.Add(new Vector3(dx, dy, dz));
            buffer.Add(new Vector3(bx, by, bz));
            buffer.Add(new Vector3(cx, cy, cz));
            //second col

            colourBuffer.Add(new Color(col, col, col));

        }

        public Mesh[] buildMeshes()
        {
            int numVerts = 60000;
            int numChunks = (int)(data.Length / numVerts)+1;
            mesh = new Mesh[numChunks];
            
            for(int i =0;i<numChunks; i++)
            {
                //make sure copy the right amount of data
                int chunkLength = (i != numChunks-1) ? numVerts : (data.Length % numVerts);
                Vector3[] chunkData = new Vector3[chunkLength];
                
                Array.Copy(data, i * numVerts, chunkData, 0, chunkLength);
                mesh[i] = buildMesh(chunkData);
            }
            
            return mesh;
        }
        private Mesh buildMesh(Vector3[] chunkData)
        {
            
            int[] tris = new int[chunkData.Length];
            Vector3[] verts = new Vector3[chunkData.Length];
            Mesh chunk = new Mesh();
            int f = 0;
            for (int i = 0; i < chunkData.Length; i += 3)
            {
                verts[i] = chunkData[i];
                verts[i+1] = chunkData[i+1];
                verts[i+2] = chunkData[i+2];
                tris[i] = i;
                tris[i + 1] = i + 1;
                tris[i + 2] = i + 2;
                //GL.Color((Color)pts[i+3]);
            }
            chunk.vertices = verts;
            chunk.triangles = tris;
            return chunk;
        }


    }
}
