﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts 
{
	
	public class Manager : MonoBehaviour 
	{
		public static int renderDist;
		public int setRenderDist;

		public static int gridWidth;
		public int setGridWidth;

		public static int gridHeight;
		public int setGridHeight;

		public static int gridDepth;
		public int setGridDepth;

		// Use this for initialization
		void Awake () 
		{
			gridWidth = setGridWidth;
			gridHeight = setGridHeight;
			gridDepth = setGridDepth;
		}
	
		// Update is called once per frame
		void Update () 
		{
			renderDist = setRenderDist;
	
		}
	}
}
