﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class CAEngine : GenericWorker
    {
        public VoxelGrid grid;
        HashSet<int> birthRules = new HashSet<int> {5,7};
        HashSet<int> survivalRules = new HashSet<int> {6};
        public int generations = 0;
        public float[] updateBuffer;

        public CAEngine(VoxelGrid _grid) :base()
        {
            grid = _grid;
        }

        public VoxelGrid getData()
        {
            return grid;
        }

        public override void run(BackgroundWorker worker)
        {
            generations++;
            updateBuffer = new float[grid.w*grid.h*grid.d];
            int index = 0;
            for (int z = 0; z < grid.d; z++)
            {
                for (int y = 0; y < grid.h; y++)
                {
                    for (int x = 0; x < grid.w; x++)
                    {
                        int sum = 0; //sum average data vals
                        float cellVal = grid.getValue(x, y, z);
                       
                        for (int i = -1; i <= 1; i++)
                        {
                            for (int j = -1; j <= 1; j++)
                            {
                                for (int k = -1; k <= 1; k++)
                                {
                                    int nx = x + i;
                                    int ny = y + j;
                                    int nz = z + k;

                                    
                                    if (grid.getValue(nx,ny, nz) > 0)
                                    {
                                        sum += 1; //add its value to the sum
                                    }
                                }
                            }
                        }

                        //ca stuff
                        if (cellVal <= 0 && birthRules.Contains(sum))
                        {
                            cellVal = 255;
                            grid.setValue(x, y, z, cellVal);
          

                        }
                        else if (cellVal >0 && !survivalRules.Contains(sum))
                        {
                            cellVal =0;
                            grid.setValue(x, y, z, cellVal);
     
                        }
                        else
                        {
                          cellVal -= 10f;
                           if (cellVal < 0) cellVal = 0;
                        }
                       // grid.setValue(x, y, z, cellVal);
                        updateBuffer[index] = cellVal;
                        index++;
                        ///otherwise do nothing
                    }
                }
                worker.ReportProgress((int)(((float)z/grid.d)*100));
            }
        }


        /*NOTE - working with cell classes - will reimplement when performance issues addressed 

        void runBehaviourOnCell(Cell c)
        {
            
            int sum = 0; //sum average data vals
            float cellVal = c.get();

            for(int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++)
                    {
                        int nx = c.x+i;
                        int ny = c.y+j;
                        int nz = c.z+k;

                         Cell neighbour = grid.getCell(nx, ny, nz);

                         if (neighbour.get() > 0) {
                              sum += 1; //add its value to the sum
                          }
                    }
                }
            }
            
            //ca stuff
            if (cellVal == 0 && birthRules.Contains(sum))
            {
                cellVal = 1;
            }
            else if (cellVal == 1 && !survivalRules.Contains(sum))
            {
                cellVal = 0;
            }
            else {
                cellVal = 0;
                //if(random(1)<0.01)remove.add(vKey);
            }
            c.set(cellVal);
           
        }
        */
    }
}
