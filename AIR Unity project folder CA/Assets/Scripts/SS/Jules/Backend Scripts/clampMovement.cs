﻿using UnityEngine;
using System.Collections;

public class clampMovement : MonoBehaviour {

	public GameObject player;
	public int offset = 25;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//transform.position = player.transform.position;
		transform.position = new Vector3 (Mathf.RoundToInt(player.transform.position.x + offset), 0, Mathf.RoundToInt(player.transform.position.z + offset));
	}
}
