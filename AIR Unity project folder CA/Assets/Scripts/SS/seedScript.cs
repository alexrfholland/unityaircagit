﻿using UnityEngine;
using System.Collections;

public class seedScript : MonoBehaviour {

	public int bodyCount;
	public int growCap;
	public bool isCaged;
	public bool canGrow;
	public GameObject blob;

	public int minGrow = 6;
	public int maxGrow = 30;



	private GameObject manager;

	// Use this for initialization
	void Awake () 
	{
		manager = GameObject.Find ("Manager");
		manager.GetComponent<SpawnCounter> ().SeedNo += 1;

		gameObject.name = "seed" + manager.GetComponent<SpawnCounter> ().SeedNo;

		growCap = Random.Range (minGrow, maxGrow);

		GameObject newObj = Instantiate (blob, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
		newObj.transform.parent = gameObject.transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
}
