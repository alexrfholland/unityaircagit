﻿using UnityEngine;
using System.Collections;

public class Colour : MonoBehaviour {

	public Transform player;
	public float accumColor = 0;
	public GameObject blob;
	public float rad = 5;
	Renderer myRenderer;

	public int attributeA;


	bool tileVis;
	public bool blobCanSpawn;

	// Use this for initialization
	void Start () 
	{
		tileVis = false;
		blobCanSpawn = true;
		myRenderer = gameObject.GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () 
	{


		float dist = Vector3.Distance (gameObject.transform.position, player.position);

		if (dist < rad) 
		{
			accumColor = accumColor + 0.03f;
		} 
		else 
		{
			accumColor = accumColor - 0.01f;
		}

		if (accumColor > 1) 
		{
			accumColor = 1;
		}

		if (accumColor < 0) 
		{
			accumColor = 0;
		}

		if (accumColor > 0) 
		{
			tileVis = true;
		}

		if (accumColor == 0) 
		{
			tileVis = false;
		}
			
		myRenderer.enabled = tileVis;
		Color myColor = new Vector4(accumColor, accumColor, accumColor, 1);
		myRenderer.material.color = myColor;

		if (accumColor == 1) 
		{	
				//int chanceSpawn = Random.Range (0, 4);

			if (blobCanSpawn == true) 
				{
					Vector3 pos = gameObject.transform.position;
					Quaternion rot = Quaternion.identity;
					Instantiate (blob, pos, rot);
					blobCanSpawn = false;

					//Invoke ("spawnBlock", 2);
				}

		}

	}
		
}
