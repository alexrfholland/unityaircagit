﻿using UnityEngine;
using System.Collections;

public class Generate2 : MonoBehaviour {

	public seedScript ScriptfromSeeder;
	private GameObject seeder;
	private GameObject manager;
	private int SeedNum;

	public GameObject obj;
	public float scale1 = 1.2f;
	public float scale2 = 0.9f;

	public int max = 5;
	public int chance = 3;

	float locX;
	float locY;
	float locZ;






	// Use this for initialization
	void Start () 
	{

		manager = GameObject.Find ("Manager");
		SeedNum = manager.GetComponent<SpawnCounter> ().SeedNo;

		seeder = GameObject.Find ("seed" +SeedNum);
		ScriptfromSeeder = seeder.GetComponent<seedScript> ();

		int spawnDir = Random.Range (0, 13);

		if (spawnDir <= 3) 
		{
			locX = 0.6f;
			locY = 0.6f;
			locZ = -0.6f;
		}

		if (spawnDir >= 4 && spawnDir <= 6) 
		{
			locX = -0.6f;
			locY = 0.6f;
			locZ = -0.6f;
		}

		if (spawnDir >7 && spawnDir <= 9) 
		{
			locX = 0.6f;
			locY = 0.6f;
			locZ = 0.6f;
		}

		if (spawnDir == 10) 
		{
			locX = 0.6f;
			locY = -0.6f;
			locZ = -0.6f;
		}

		if (spawnDir == 11) 
		{
			locX = -0.6f;
			locY = -0.6f;
			locZ = -0.6f;
		}

		if (spawnDir == 12) 
		{
			locX = 0.6f;
			locY = -0.6f;
			locZ = 0.6f;
		}


		ScriptfromSeeder.bodyCount += 1;


		if (ScriptfromSeeder.bodyCount < ScriptfromSeeder.growCap) 
		{
			Invoke ("Grow", .3f);
		}


		//Invoke ("Die", 3);



	}

	void Grow()
	{
		int regen = Random.Range (0, max);

		if (regen < chance) 
		{
			Vector3 dir = new Vector3 (locX, locY, locZ);
			Vector3 pos = gameObject.transform.position + dir;
			Quaternion rot = Quaternion.identity;
			Instantiate (obj, pos, rot);	
		}

	}

	void Die()
	{
		Destroy (this.gameObject);
	}

}

